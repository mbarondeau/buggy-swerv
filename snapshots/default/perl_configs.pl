#  NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE
#  This is an automatically generated file by matthew on Tue 21 Apr 2020 09:18:09 AM CDT
# 
#  cmd:    swerv -target=default 
# 
# To use this in a perf script, use 'require $RV_ROOT/configs/config.pl'
# Reference the hash via $config{name}..


%config = (
            'verilator' => '',
            'regwidth' => '32',
            'memmap' => {
                          'debug_sb_mem' => '0xb0580000',
                          'external_data_1' => '0x00000000',
                          'serialio' => '0xd0580000',
                          'external_data' => '0xc0580000',
                          'external_prog' => '0xb0000000'
                        },
            'protection' => {
                              'data_access_enable0' => '0x0',
                              'inst_access_enable6' => '0x0',
                              'data_access_enable6' => '0x0',
                              'inst_access_enable0' => '0x0',
                              'data_access_mask4' => '0xffffffff',
                              'inst_access_mask4' => '0xffffffff',
                              'inst_access_addr0' => '0x00000000',
                              'data_access_mask1' => '0xffffffff',
                              'inst_access_addr5' => '0x00000000',
                              'data_access_addr5' => '0x00000000',
                              'inst_access_mask1' => '0xffffffff',
                              'data_access_addr0' => '0x00000000',
                              'data_access_mask2' => '0xffffffff',
                              'inst_access_mask2' => '0xffffffff',
                              'data_access_enable1' => '0x0',
                              'inst_access_addr4' => '0x00000000',
                              'data_access_addr4' => '0x00000000',
                              'inst_access_enable1' => '0x0',
                              'inst_access_addr2' => '0x00000000',
                              'data_access_addr2' => '0x00000000',
                              'data_access_mask5' => '0xffffffff',
                              'inst_access_addr1' => '0x00000000',
                              'data_access_mask0' => '0xffffffff',
                              'inst_access_mask0' => '0xffffffff',
                              'data_access_addr1' => '0x00000000',
                              'inst_access_mask5' => '0xffffffff',
                              'inst_access_mask7' => '0xffffffff',
                              'inst_access_enable4' => '0x0',
                              'data_access_enable4' => '0x0',
                              'data_access_mask7' => '0xffffffff',
                              'inst_access_enable7' => '0x0',
                              'data_access_mask6' => '0xffffffff',
                              'inst_access_enable5' => '0x0',
                              'data_access_enable3' => '0x0',
                              'data_access_enable5' => '0x0',
                              'inst_access_enable3' => '0x0',
                              'data_access_enable7' => '0x0',
                              'inst_access_mask6' => '0xffffffff',
                              'data_access_enable2' => '0x0',
                              'inst_access_addr3' => '0x00000000',
                              'data_access_addr3' => '0x00000000',
                              'inst_access_enable2' => '0x0',
                              'data_access_addr7' => '0x00000000',
                              'inst_access_addr7' => '0x00000000',
                              'data_access_mask3' => '0xffffffff',
                              'inst_access_mask3' => '0xffffffff',
                              'inst_access_addr6' => '0x00000000',
                              'data_access_addr6' => '0x00000000'
                            },
            'physical' => '1',
            'triggers' => [
                            {
                              'poke_mask' => [
                                               '0x081818c7',
                                               '0xffffffff',
                                               '0x00000000'
                                             ],
                              'reset' => [
                                           '0x23e00000',
                                           '0x00000000',
                                           '0x00000000'
                                         ],
                              'mask' => [
                                          '0x081818c7',
                                          '0xffffffff',
                                          '0x00000000'
                                        ]
                            },
                            {
                              'poke_mask' => [
                                               '0x081818c7',
                                               '0xffffffff',
                                               '0x00000000'
                                             ],
                              'mask' => [
                                          '0x081818c7',
                                          '0xffffffff',
                                          '0x00000000'
                                        ],
                              'reset' => [
                                           '0x23e00000',
                                           '0x00000000',
                                           '0x00000000'
                                         ]
                            },
                            {
                              'poke_mask' => [
                                               '0x081818c7',
                                               '0xffffffff',
                                               '0x00000000'
                                             ],
                              'reset' => [
                                           '0x23e00000',
                                           '0x00000000',
                                           '0x00000000'
                                         ],
                              'mask' => [
                                          '0x081818c7',
                                          '0xffffffff',
                                          '0x00000000'
                                        ]
                            },
                            {
                              'reset' => [
                                           '0x23e00000',
                                           '0x00000000',
                                           '0x00000000'
                                         ],
                              'mask' => [
                                          '0x081818c7',
                                          '0xffffffff',
                                          '0x00000000'
                                        ],
                              'poke_mask' => [
                                               '0x081818c7',
                                               '0xffffffff',
                                               '0x00000000'
                                             ]
                            }
                          ],
            'core' => {
                        'lsu_num_nbload' => '8',
                        'dec_instbuf_depth' => '4',
                        'lsu_stbuf_depth' => '8',
                        'lsu_num_nbload_width' => '3',
                        'dma_buf_depth' => '4'
                      },
            'xlen' => 32,
            'dccm' => {
                        'dccm_num_banks' => '8',
                        'lsu_sb_bits' => 16,
                        'dccm_ecc_width' => 7,
                        'dccm_width_bits' => 2,
                        'dccm_index_bits' => 11,
                        'dccm_size_64' => '',
                        'dccm_size' => 64,
                        'dccm_byte_width' => '4',
                        'dccm_reserved' => '0x1000',
                        'dccm_rows' => '2048',
                        'dccm_num_banks_8' => '',
                        'dccm_bank_bits' => 3,
                        'dccm_eadr' => '0xf004ffff',
                        'dccm_data_cell' => 'ram_2048x39',
                        'dccm_sadr' => '0xf0040000',
                        'dccm_bits' => 16,
                        'dccm_data_width' => 32,
                        'dccm_fdata_width' => 39,
                        'dccm_region' => '0xf',
                        'dccm_offset' => '0x40000',
                        'dccm_enable' => '1'
                      },
            'iccm' => {
                        'iccm_offset' => '0xe000000',
                        'iccm_bits' => 19,
                        'iccm_region' => '0xe',
                        'iccm_sadr' => '0xee000000',
                        'iccm_data_cell' => 'ram_16384x39',
                        'iccm_bank_bits' => 3,
                        'iccm_eadr' => '0xee07ffff',
                        'iccm_num_banks_8' => '',
                        'iccm_rows' => '16384',
                        'iccm_size_512' => '',
                        'iccm_reserved' => '0x1000',
                        'iccm_size' => 512,
                        'iccm_index_bits' => 14,
                        'iccm_num_banks' => '8'
                      },
            'icache' => {
                          'icache_ic_depth' => 8,
                          'icache_enable' => '1',
                          'icache_tag_depth' => 64,
                          'icache_tag_high' => 12,
                          'icache_data_cell' => 'ram_256x34',
                          'icache_tag_low' => '6',
                          'icache_taddr_high' => 5,
                          'icache_ic_index' => 8,
                          'icache_ic_rows' => '256',
                          'icache_size' => 16,
                          'icache_tag_cell' => 'ram_64x21'
                        },
            'numiregs' => '32',
            'harts' => 1,
            'bht' => {
                       'bht_ghr_pad2' => 'fghr[4:3],2\'b0',
                       'bht_hash_string' => '{ghr[3:2] ^ {ghr[3+1], {4-1-2{1\'b0} } },hashin[5:4]^ghr[2-1:0]}',
                       'bht_addr_lo' => '4',
                       'bht_ghr_pad' => 'fghr[4],3\'b0',
                       'bht_ghr_range' => '4:0',
                       'bht_array_depth' => 16,
                       'bht_ghr_size' => 5,
                       'bht_size' => 128,
                       'bht_addr_hi' => 7
                     },
            'tec_rv_icg' => 'clockhdr',
            'pic' => {
                       'pic_int_words' => 1,
                       'pic_size' => 32,
                       'pic_mpiccfg_offset' => '0x3000',
                       'pic_meie_offset' => '0x2000',
                       'pic_meipl_offset' => '0x0000',
                       'pic_total_int' => 8,
                       'pic_meip_offset' => '0x1000',
                       'pic_base_addr' => '0xf00c0000',
                       'pic_meigwclr_offset' => '0x5000',
                       'pic_offset' => '0xc0000',
                       'pic_region' => '0xf',
                       'pic_meipt_offset' => '0x3004',
                       'pic_bits' => 15,
                       'pic_meigwctrl_offset' => '0x4000',
                       'pic_total_int_plus1' => 9
                     },
            'csr' => {
                       'mhpmevent5' => {
                                         'exists' => 'true',
                                         'mask' => '0xffffffff',
                                         'reset' => '0x0'
                                       },
                       'miccmect' => {
                                       'mask' => '0xffffffff',
                                       'exists' => 'true',
                                       'number' => '0x7f1',
                                       'reset' => '0x0'
                                     },
                       'mhpmcounter5h' => {
                                            'exists' => 'true',
                                            'mask' => '0xffffffff',
                                            'reset' => '0x0'
                                          },
                       'misa' => {
                                   'exists' => 'true',
                                   'mask' => '0x0',
                                   'reset' => '0x40001104'
                                 },
                       'mimpid' => {
                                     'reset' => '0x2',
                                     'exists' => 'true',
                                     'mask' => '0x0'
                                   },
                       'pmpaddr9' => {
                                       'exists' => 'false'
                                     },
                       'marchid' => {
                                      'mask' => '0x0',
                                      'exists' => 'true',
                                      'reset' => '0x0000000b'
                                    },
                       'micect' => {
                                     'number' => '0x7f0',
                                     'reset' => '0x0',
                                     'mask' => '0xffffffff',
                                     'exists' => 'true'
                                   },
                       'mstatus' => {
                                      'mask' => '0x88',
                                      'exists' => 'true',
                                      'reset' => '0x1800'
                                    },
                       'pmpcfg3' => {
                                      'exists' => 'false'
                                    },
                       'mcgc' => {
                                   'poke_mask' => '0x000001ff',
                                   'reset' => '0x0',
                                   'number' => '0x7f8',
                                   'mask' => '0x000001ff',
                                   'exists' => 'true'
                                 },
                       'mhpmevent6' => {
                                         'reset' => '0x0',
                                         'mask' => '0xffffffff',
                                         'exists' => 'true'
                                       },
                       'pmpaddr0' => {
                                       'exists' => 'false'
                                     },
                       'mhpmevent3' => {
                                         'reset' => '0x0',
                                         'exists' => 'true',
                                         'mask' => '0xffffffff'
                                       },
                       'pmpaddr8' => {
                                       'exists' => 'false'
                                     },
                       'pmpaddr12' => {
                                        'exists' => 'false'
                                      },
                       'meicidpl' => {
                                       'reset' => '0x0',
                                       'number' => '0xbcb',
                                       'comment' => 'External interrupt claim id priority level.',
                                       'mask' => '0xf',
                                       'exists' => 'true'
                                     },
                       'mip' => {
                                  'poke_mask' => '0x40000888',
                                  'exists' => 'true',
                                  'mask' => '0x0',
                                  'reset' => '0x0'
                                },
                       'dcsr' => {
                                   'poke_mask' => '0x00008dcc',
                                   'reset' => '0x40000003',
                                   'exists' => 'true',
                                   'mask' => '0x00008c04'
                                 },
                       'instret' => {
                                      'exists' => 'false'
                                    },
                       'mhpmcounter5' => {
                                           'reset' => '0x0',
                                           'exists' => 'true',
                                           'mask' => '0xffffffff'
                                         },
                       'pmpaddr14' => {
                                        'exists' => 'false'
                                      },
                       'pmpaddr5' => {
                                       'exists' => 'false'
                                     },
                       'pmpaddr6' => {
                                       'exists' => 'false'
                                     },
                       'pmpaddr4' => {
                                       'exists' => 'false'
                                     },
                       'pmpaddr2' => {
                                       'exists' => 'false'
                                     },
                       'meipt' => {
                                    'comment' => 'External interrupt priority threshold.',
                                    'reset' => '0x0',
                                    'number' => '0xbc9',
                                    'exists' => 'true',
                                    'mask' => '0xf'
                                  },
                       'pmpaddr11' => {
                                        'exists' => 'false'
                                      },
                       'mhpmevent4' => {
                                         'reset' => '0x0',
                                         'exists' => 'true',
                                         'mask' => '0xffffffff'
                                       },
                       'dicawics' => {
                                       'number' => '0x7c8',
                                       'reset' => '0x0',
                                       'comment' => 'Cache diagnostics.',
                                       'debug' => 'true',
                                       'mask' => '0x0130fffc',
                                       'exists' => 'true'
                                     },
                       'mvendorid' => {
                                        'mask' => '0x0',
                                        'exists' => 'true',
                                        'reset' => '0x45'
                                      },
                       'dicad1' => {
                                     'debug' => 'true',
                                     'comment' => 'Cache diagnostics.',
                                     'reset' => '0x0',
                                     'number' => '0x7ca',
                                     'exists' => 'true',
                                     'mask' => '0x3'
                                   },
                       'dicago' => {
                                     'reset' => '0x0',
                                     'comment' => 'Cache diagnostics.',
                                     'number' => '0x7cb',
                                     'debug' => 'true',
                                     'exists' => 'true',
                                     'mask' => '0x0'
                                   },
                       'pmpaddr1' => {
                                       'exists' => 'false'
                                     },
                       'mhpmcounter4' => {
                                           'reset' => '0x0',
                                           'mask' => '0xffffffff',
                                           'exists' => 'true'
                                         },
                       'mhpmcounter6' => {
                                           'mask' => '0xffffffff',
                                           'exists' => 'true',
                                           'reset' => '0x0'
                                         },
                       'mhpmcounter3' => {
                                           'reset' => '0x0',
                                           'exists' => 'true',
                                           'mask' => '0xffffffff'
                                         },
                       'tselect' => {
                                      'reset' => '0x0',
                                      'exists' => 'true',
                                      'mask' => '0x3'
                                    },
                       'pmpaddr7' => {
                                       'exists' => 'false'
                                     },
                       'meicpct' => {
                                      'number' => '0xbca',
                                      'reset' => '0x0',
                                      'comment' => 'External claim id/priority capture.',
                                      'mask' => '0x0',
                                      'exists' => 'true'
                                    },
                       'time' => {
                                   'exists' => 'false'
                                 },
                       'dicad0' => {
                                     'reset' => '0x0',
                                     'number' => '0x7c9',
                                     'comment' => 'Cache diagnostics.',
                                     'debug' => 'true',
                                     'mask' => '0xffffffff',
                                     'exists' => 'true'
                                   },
                       'mdccmect' => {
                                       'reset' => '0x0',
                                       'number' => '0x7f2',
                                       'mask' => '0xffffffff',
                                       'exists' => 'true'
                                     },
                       'pmpaddr3' => {
                                       'exists' => 'false'
                                     },
                       'pmpaddr15' => {
                                        'exists' => 'false'
                                      },
                       'cycle' => {
                                    'exists' => 'false'
                                  },
                       'mfdc' => {
                                   'exists' => 'true',
                                   'mask' => '0x000707ff',
                                   'reset' => '0x00070000',
                                   'number' => '0x7f9'
                                 },
                       'mhpmcounter6h' => {
                                            'reset' => '0x0',
                                            'exists' => 'true',
                                            'mask' => '0xffffffff'
                                          },
                       'mhpmcounter3h' => {
                                            'reset' => '0x0',
                                            'exists' => 'true',
                                            'mask' => '0xffffffff'
                                          },
                       'mie' => {
                                  'exists' => 'true',
                                  'mask' => '0x40000888',
                                  'reset' => '0x0'
                                },
                       'pmpaddr10' => {
                                        'exists' => 'false'
                                      },
                       'mcpc' => {
                                   'reset' => '0x0',
                                   'number' => '0x7c2',
                                   'mask' => '0x0',
                                   'exists' => 'true'
                                 },
                       'mhpmcounter4h' => {
                                            'reset' => '0x0',
                                            'mask' => '0xffffffff',
                                            'exists' => 'true'
                                          },
                       'pmpcfg0' => {
                                      'exists' => 'false'
                                    },
                       'pmpaddr13' => {
                                        'exists' => 'false'
                                      },
                       'dmst' => {
                                   'comment' => 'Memory synch trigger: Flush caches in debug mode.',
                                   'reset' => '0x0',
                                   'number' => '0x7c4',
                                   'debug' => 'true',
                                   'exists' => 'true',
                                   'mask' => '0x0'
                                 },
                       'meicurpl' => {
                                       'exists' => 'true',
                                       'mask' => '0xf',
                                       'reset' => '0x0',
                                       'comment' => 'External interrupt current priority level.',
                                       'number' => '0xbcc'
                                     },
                       'mpmc' => {
                                   'reset' => '0x0',
                                   'comment' => 'Core pause: Implemented as read only.',
                                   'number' => '0x7c6',
                                   'exists' => 'true',
                                   'mask' => '0x0'
                                 },
                       'pmpcfg2' => {
                                      'exists' => 'false'
                                    },
                       'pmpcfg1' => {
                                      'exists' => 'false'
                                    }
                     },
            'retstack' => {
                            'ret_stack_size' => '4'
                          },
            'reset_vec' => '0x80000000',
            'btb' => {
                       'btb_size' => 32,
                       'btb_index2_lo' => 6,
                       'btb_index3_hi' => 9,
                       'btb_index1_lo' => '4',
                       'btb_addr_lo' => '4',
                       'btb_btag_fold' => 1,
                       'btb_addr_hi' => 5,
                       'btb_index1_hi' => 5,
                       'btb_array_depth' => 4,
                       'btb_index3_lo' => 8,
                       'btb_index2_hi' => 7,
                       'btb_btag_size' => 9
                     },
            'max_mmode_perf_event' => '50',
            'target' => 'default',
            'testbench' => {
                             'assert_on' => '',
                             'ext_addrwidth' => '32',
                             'lderr_rollback' => '1',
                             'ext_datawidth' => '64',
                             'CPU_TOP' => '`RV_TOP.swerv',
                             'RV_TOP' => '`TOP.rvtop',
                             'SDVT_AHB' => '1',
                             'datawidth' => '64',
                             'TOP' => 'tb_top',
                             'clock_period' => '100',
                             'build_axi4' => '1',
                             'sterr_rollback' => '0'
                           },
            'even_odd_trigger_chains' => 'true',
            'bus' => {
                       'dma_bus_tag' => '1',
                       'sb_bus_tag' => '1',
                       'lsu_bus_tag' => 4,
                       'ifu_bus_tag' => '3'
                     },
            'num_mmode_perf_regs' => '4',
            'nmi_vec' => '0x11110000'
          );
1;
